package language

import (
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/assembly"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/ats"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/bash"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/clang-c"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/clang-cpp"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/clojure"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/cobol"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/coffeescript"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/crystal"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/csharp"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/d"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/elixir"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/elm"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/erlang"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/fsharp"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/gcc-c"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/gcc-cpp"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/golang"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/groovy"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/haskell"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/idris"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/java"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/javascript"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/julia"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/kotlin"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/lua"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/mercury"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/nim"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/ocaml"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/perl"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/perl6"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/php"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/python"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/ruby"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/rust"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/scala"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/swift"
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/language/typescript"
)

type runFn func([]string, string) (string, string, error)

var languages = map[string]runFn{
	"assembly":     assembly.Run,
	"ats":          ats.Run,
	"bash":         bash.Run,
	"c":            clang_c.Run,
	"c-clang":      clang_c.Run,
	"c-gcc":        gcc_c.Run,
	"clojure":      clojure.Run,
	"cobol":        cobol.Run,
	"coffeescript": coffeescript.Run,
	"crystal":      crystal.Run,
	"csharp":       csharp.Run,
	"d":            d.Run,
	"elixir":       elixir.Run,
	"elm":          elm.Run,
	"cpp":          clang_cpp.Run,
	"cpp-clang":    clang_cpp.Run,
	"cpp-gcc":      gcc_cpp.Run,
	"erlang":       erlang.Run,
	"fsharp":       fsharp.Run,
	"haskell":      haskell.Run,
	"idris":        idris.Run,
	"go":           golang.Run,
	"groovy":       groovy.Run,
	"java":         java.Run,
	"javascript":   javascript.Run,
	"julia":        julia.Run,
	"kotlin":       kotlin.Run,
	"lua":          lua.Run,
	"mercury":      mercury.Run,
	"nim":          nim.Run,
	"ocaml":        ocaml.Run,
	"perl":         perl.Run,
	"perl6":        perl6.Run,
	"php":          php.Run,
	"python":       python.Run,
	"ruby":         ruby.Run,
	"rust":         rust.Run,
	"scala":        scala.Run,
	"swift":        swift.Run,
	"typescript":   typescript.Run,
}

type testFn func([]string, string) (string, string, error)

var testLanguages = map[string]testFn{
	"assembly":     assembly.RunTests,
	"ats":          ats.RunTests,
	"bash":         bash.RunTests,
	"c":            clang_c.RunTests,
	"c-clang":      clang_c.RunTests,
	"c-gcc":        gcc_c.RunTests,
	"clojure":      clojure.RunTests,
	"cobol":        cobol.RunTests,
	"coffeescript": coffeescript.RunTests,
	"crystal":      crystal.RunTests,
	"csharp":       csharp.RunTests,
	"d":            d.RunTests,
	"elixir":       elixir.RunTests,
	"elm":          elm.RunTests,
	"cpp":          clang_cpp.RunTests,
	"cpp-clang":    clang_cpp.RunTests,
	"cpp-gcc":      gcc_cpp.RunTests,
	"erlang":       erlang.RunTests,
	"fsharp":       fsharp.RunTests,
	"haskell":      haskell.RunTests,
	"idris":        idris.RunTests,
	"go":           golang.RunTests,
	"groovy":       groovy.RunTests,
	"java":         java.RunTests,
	"javascript":   javascript.RunTests,
	"julia":        julia.RunTests,
	"kotlin":       kotlin.RunTests,
	"lua":          lua.RunTests,
	"mercury":      mercury.RunTests,
	"nim":          nim.RunTests,
	"ocaml":        ocaml.RunTests,
	"perl":         perl.RunTests,
	"perl6":        perl6.RunTests,
	"php":          php.RunTests,
	"python":       python.RunTests,
	"ruby":         ruby.RunTests,
	"rust":         rust.RunTests,
	"scala":        scala.RunTests,
	"swift":        swift.RunTests,
	"typescript":   typescript.RunTests,
}

type compileFn func([]string, string) (string, string, error)

var compileLanguages = map[string]compileFn{
	"assembly":     assembly.Compile,
	"ats":          ats.Compile,
	"bash":         bash.Compile,
	"c":            clang_c.Compile,
	"c-clang":      clang_c.Compile,
	"c-gcc":        gcc_c.Compile,
	"clojure":      clojure.Compile,
	"cobol":        cobol.Compile,
	"coffeescript": coffeescript.Compile,
	"crystal":      crystal.Compile,
	"csharp":       csharp.Compile,
	"d":            d.Compile,
	"elixir":       elixir.Compile,
	"elm":          elm.Compile,
	"cpp":          clang_cpp.Compile,
	"cpp-clang":    clang_cpp.Compile,
	"cpp-gcc":      gcc_cpp.Compile,
	"erlang":       erlang.Compile,
	"fsharp":       fsharp.Compile,
	"haskell":      haskell.Compile,
	"idris":        idris.Compile,
	"go":           golang.Compile,
	"groovy":       groovy.Compile,
	"java":         java.Compile,
	"javascript":   javascript.Compile,
	"julia":        julia.Compile,
	"kotlin":       kotlin.Compile,
	"lua":          lua.Compile,
	"mercury":      mercury.Compile,
	"nim":          nim.Compile,
	"ocaml":        ocaml.Compile,
	"perl":         perl.Compile,
	"perl6":        perl6.Compile,
	"php":          php.Compile,
	"python":       python.Compile,
	"ruby":         ruby.Compile,
	"rust":         rust.Compile,
	"scala":        scala.Compile,
	"swift":        swift.Compile,
	"typescript":   typescript.Compile,
}

func IsSupported(lang string) bool {
	_, supported := languages[lang]
	return supported
}

func Run(lang string, files []string, stdin string) (string, string, error) {
	return languages[lang](files, stdin)
}

func Compile(lang string, files []string, stdin string) (string, string, error) {
	return compileLanguages[lang](files, stdin)
}

func Test(lang string, files []string, stdin string) (string, string, error) {
	return testLanguages[lang](files, stdin)
}

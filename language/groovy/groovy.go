package groovy

import (
	"gitlab.com/kennethsohyq/school/university/fyp/code-runner/v2/cmd"
	"path/filepath"
)

func Run(files []string, stdin string) (string, string, error) {
	return RunTests(files, stdin)
}

func RunTests(files []string, stdin string) (string, string, error) {
	workDir := filepath.Dir(files[0])
	// TODO: Figure out how to implement memory limit
	return cmd.RunStdinTestCPU(workDir, stdin, "groovy", files[0])
}

func Compile(files []string, stdin string) (string, string, error) {
	return "", "", nil // NO-OP
}
package util

import (
	"net/http"
	"os"
	"path/filepath"
	"strconv"
)

var Memlimit int // in KB
var Cpulimit int

func FilterByExtension(files []string, ext string) []string {
	var newFiles []string
	suffix := "." + ext

	for _, file := range files {
		if filepath.Ext(file) == suffix {
			newFiles = append(newFiles, file)
		}
	}

	return newFiles
}

func UpdateEnvLimits() {
	//ml := GetEnvDefaultInt("MEM_LIMIT", 1024) // Hard 1GB fallback if memory limit not defined by master controller
	Memlimit = GetEnvDefaultInt("MEM_LIMIT", 1024) * 1000 // Hard 1GB fallback if memory limit not defined by master controller
//	Memlimit = ml * 1000 // 1GB
	Cpulimit = GetEnvDefaultInt("CPU_TIME_LIMIT", 60) // Hard 60 seconds fallback if CPU time limit not defined by master controller
}

func GetEnvDefault(key string, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}

func GetEnvDefaultInt(key string, fallback int) int {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	val, err := strconv.Atoi(value)
	if err != nil {
		return fallback
	}
	return val
}

func GetFileContentType(out *os.File) (string, error) {

	// Only the first 512 bytes are used to sniff the content type.
	buffer := make([]byte, 512)

	_, err := out.Read(buffer)
	if err != nil {
		return "", err
	}

	// Use the net/http package's handy DectectContentType function. Always returns a valid
	// content-type by returning "application/octet-stream" if no others seemed to match.
	contentType := http.DetectContentType(buffer)

	return contentType, nil
}